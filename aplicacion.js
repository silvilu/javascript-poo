class Persona{

    constructor(nombre,edad,fecha){
        this.nombre = nombre,
        this.edad   = edad,
        this.fecha  = fecha
    }
}
class Interface{

    listaPersonal(objpersona){
        let contenedorPer = document.getElementById("listadousuarios");
        let contenedor = document.createElement("div");
        contenedor.innerHTML = `
         <div class="card text-center">

            <div class="card-body">

                <strong>Nombre:</strong> ${objpersona.nombre}
                <strong>Edad:</strong> ${objpersona.edad}
                <strong>Fecha:</strong> ${objpersona.fecha}
                <button class="btn btn-danger" id="Eliminar" name="delete">
                  Eliminar
                </button>

            </div>
         </div>


        `;


       contenedorPer.appendChild(contenedor);


    };

    limpiarformulario(){
        document.getElementById("nuevopersonal").reset();

    }

    eliminarCard(valor){

       if( valor.name == "delete"){
            valor.parentElement.parentElement.parentElement.remove();
       }

    
    }


}

//DOM del navegador
document.getElementById("nuevopersonal").addEventListener('submit',function (e){
    e.preventDefault();
    
    let nombre = document.getElementById("nombre").value;
    let edad = document.getElementById("edad").value;
    let fecha = document.getElementById("fecharegistro").value;
    
    const newpers = new Persona(nombre,edad,fecha);
    const InterfacePersona = new Interface();
    InterfacePersona.listaPersonal(newpers);
    InterfacePersona.limpiarformulario();

//console.log('newpers : ',newpers);

})

document.getElementById("listadousuarios").addEventListener('click',function (e)
{

 let eliminar = e.target;
 const Interfacei = new Interface();
 Interfacei.eliminarCard(eliminar);
 //console.log("eliminado" , eliminar);
 

   // e.preventDefault();
 //  console.log("eliminado" , eliminar);
   

})